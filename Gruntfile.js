module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    bower: {
      install: {
        options: {
          targetDir: 'src/lib/',
          cleanTargetDir: false,
          layout: function(type, component, source) {
              return type;
          }
        }
      }
    },
    sass: {
      dist: {
        files: {
          'src/css/master.css': ['src/css/sass/styles.scss']
        }
      },
      foundation: {
        files: {
          'src/lib/css/foundation.css': ['src/lib/scss/foundation.scss']
        }
      }
    },
    watch: {
      files: ['src/css/sass/*.scss', 'src/lib/scss/*.scss'],
      tasks: ['sass']
    },
    connect: {
      dev: {
        options: {
          port: 8000,
          base: 'src'
        }
      }
    }
  });

  grunt.registerTask('default', [
    'watch'
  ]);
};
