define([
  'knockout'
], function(ko) {

  var Item = function(id, name, photoUrl, icon, marker, infowindow) {
    this.id = id;
    this.name = name;
    this.photoUrl = photoUrl;
    this.icon = icon;

    this.marker = marker;
    this.infowindow = infowindow;
  };

  return Item;
});
