define([
	'knockout',
  'models/Item',
	'errorHandler'
], function(ko, Item, ErrorHandler) {
  'use strict';

  var MapViewModel = function(mapConfig) {
      var self = this;

			self.errHandler = new ErrorHandler();

			// Map object
			self.map = new google.maps.Map(document.getElementById('map'), {
				center: mapConfig.location,
				zoom: mapConfig.zoom,
				styles: mapConfig.styles,
				scrollwheel: mapConfig.scrollwheel,
				streetViewControl: mapConfig.streetViewControl,
				mapTypeControl: mapConfig.mapTypeControl
			});

			// Foursquare configuration objects (use for any foursquare API calls)
			self.fqConfig = {
				client_id: "DT4UI3SBC2AF4W2KIOESSANZEQXFPUEX0ICYRYJNI3NAMIU5",
				client_secret: "YAVWWGXIFBPVJOKIA3YAQ22VXVWDWTP1MGWTCIQ3QNMOJ3A3",
				v: "20170420",
				m: "foursquare",
			};

			// List of all items that have been retrieved during the initialization
      self.items = ko.observableArray();

			// Item currently selected
      self.currentItem = ko.observable();

			// Text in the search input
			self.searchQuery = ko.observable("");

			// List of items to display on the map according to the search query
			self.searchResults = ko.computed(function() {

				// List all items where searchQuery is found in their names
	    	var visibleItems = self.items().filter(function(item) {

					var query = self.searchQuery().toLowerCase();

					/* If the item doesn't contain the searchQuery :
								infowindow is closed
								marker is removed of the map
								item is not included to the searchResults
							Else :
								marker is set on the map
								item is included to the searchResults
					*/
					if (!(item.name.toLowerCase().indexOf(query) >= 0)) {
						item.infowindow.close();
						item.marker.setMap(null);

						return false;
					} else {
						item.marker.setMap(self.map);

						return true;
					}
				});

				return visibleItems;
	    }, self);


			// Functions
			self.init = function() {
				// Get Places from Foursquare API
				$.getJSON("https://api.foursquare.com/v2/venues/explore", {
										client_id: self.fqConfig.client_id,
										client_secret: self.fqConfig.client_secret,
										v: self.fqConfig.v,
										m: self.fqConfig.m,
										// Location is set to be the center of the map
										ll: self.map.getCenter().lat() +','+ self.map.getCenter().lng(),
										/* Retrieve TopPicks places (max 50) that have :
										 			- a photo
													- is currently open (during the initialization)
										*/
										section: "topPicks",
										radius: 10000,
										limit: 50,
										venuePhotos: 1,
										openNow: 1
									}
					).done(function(response) {
						// SUCCESS: Creations of items
						if (response.meta.code === 200) {
							self.createItems(response.response.groups[0].items);
						} else {
							self.errHandler.displayError(response, "Unable to retrieve places");
						}
					}).fail(function(response) {
						// FAIL: No places retrieved
						self.errHandler.displayError(response, "Unable to retrieve places");
					});

			};

			self.createItems = function (results) {
					for (var i = 0; i < results.length; i++) {
						(function (item) {
							/* For each places retrieved by Foursquare API call:
										We created an item object that contains :
											- unique ID
											- name
											- cover picture
											- category icon
											- marker
											- infowindow
							*/

							var newItem, photoUrl, icon, marker, infowindow;


							photoUrl = self.getPhotoUrl(item.photos.groups[0].items[0], "400");
							icon = self.getIconUrl(item.categories[0].icon, "32");
							marker = self.createMarker(item);
							infowindow = self.createInfoWindow();

							newItem = new Item(item.id,
																 item.name,
																 photoUrl,
																 icon,
					                       marker,
																 infowindow);

							// Select the item when user click on the marker
							newItem.marker.addListener('click', function() {
								self.selectItem(newItem);
							});

							// Add to the list of items
							self.items.push(newItem);

						}(results[i].venue));
					}
			}

      self.selectItem = function(item) {
				if (item != self.currentItem()) {
					// Close current marker
					self.deactivateMarker();

					// Set the new current item
					self.currentItem(item);

					// Change sidebar background and center the map to the position of the new item
					self.changeBackground();
					self.centerMap();

					// Open new marker and retrieve details of the place from Foursquare
					self.activateMarker();
					self.getPlaceDetails();
				}
      };

			self.getPlaceDetails = function() {
				var item = self.currentItem();

				// Get place details from Foursquare API
				$.getJSON("https://api.foursquare.com/v2/venues/"+ item.id,
						{
							client_id: self.fqConfig.client_id,
							client_secret: self.fqConfig.client_secret,
							v: self.fqConfig.v,
							m: self.fqConfig.m,
						}
					).done(function(response) {
						// Update the infowindow with datas
						if (response.meta.code === 200) {
							self.updatePlaceDetails(response.response.venue);
						} else {
							self.errHandler.displayError(response, "Unable to get place details", "#place-details");
						}
					}).fail(function(response) {
						self.errHandler.displayError(response, "Unable to get place details", "#place-details");
					});
			};

			self.updatePlaceDetails = function(item) {
				/* Update the infowindow with place details
						- name
						- category
						- address
						- description
						- thumbnails
				*/
				var photo, photoSource, thumbnailUrl, thumbnailSize, thumbnailSection;
				var placeName = $('#place-name');
				var placeCategory = $('#place-category');
				var placeAddress = $('#place-address');
				var placeDescription = $('#place-description');
				var placeThumbnails = $('#place-thumbnails');
				var placePicture = $('#place-picture');

				placeName.html("<img src='"+ self.getIconUrl(item.categories[0].icon, "44") +"'> " + item.name);
				placeCategory.html(item.categories[0].name);
				placeAddress.html(item.location.formattedAddress.join("<br>"));

				// Hide description if not available
				if (item.description) {
					placeDescription.html("<strong>Description: </strong> "+item.description);
				} else {
					placeDescription.hide();
				}

				// List of clickable thumbnails (open in new window)
				thumbnailSize = 50;
				thumbnailSection = '';
				if (item.photos.groups[0]) {
					for (var i = 0; i < item.photos.groups[0].items.length; i++) {
						photo = item.photos.groups[0].items[i];

						photoSource = self.getPhotoSource(photo);
						thumbnailUrl = self.getPhotoUrl(photo, thumbnailSize);
						thumbnailSection = thumbnailSection + "<a href='"+ self.getPhotoUrl(photo) +"' title='"+ photoSource +"' target='_blank'>"+
																"<img src='"+ thumbnailUrl +"' '></a>";
					}
				}

				placeThumbnails.html(thumbnailSection);
			};


			self.getIconUrl = function(icon, size="32") {
				// Generate the icon URL with the Category Response from Foursquare
				var iconUrl = '';

				if (!["32", "44", "64", "88"].includes(size.toString())) {
					size = "32";
				}

				if (icon && icon.prefix && icon.suffix) {
					iconUrl = icon.prefix + "bg_" + size.toString() + icon.suffix;
				}

				return iconUrl;
			};

			self.getPhotoUrl = function(photo, width=null, height=null) {
				// Generate photo URL with the Photo Response from Foursquare
				var photoUrl = '';

				if (width === null && height === null) {
					width = photo.width;
					height = photo.height;
				}

				if (height === null) {
					height = width;
				}

				if (photo && photo.prefix && photo.suffix) {
					photoUrl = photo.prefix + width.toString() + 'x' + height.toString() + photo.suffix;
				}

				return photoUrl;
			}

			self.getPhotoSource = function(photo) {
				// Generate photo source with the User Response from Foursquare
				var photoSource = '';

				if (photo.user) {
					photoSource = "Photo by ";
					if (photo.user.firstName) {
						photoSource += photo.user.firstName + " ";
					}
					if (photo.user.lastName) {
						photoSource += photo.user.lastName;
					}
				}

				return photoSource;
			}


			self.centerMap = function() {
				// Center the map according to the selected marker position
				var location = self.currentItem().marker.getPosition();

				self.map.panTo(location);
			};

			self.createMarker = function(item) {
				// Generate a marker at the item position
				return new google.maps.Marker({
		      position: { lat: item.location.lat, lng: item.location.lng },
		      map: null,
		      title: item.name,
		      icon: "http://maps.google.com/mapfiles/ms/micons/blue-dot.png"
		    });
			};

			self.createInfoWindow = function() {
				// Generate an infowindow with a template
				return new google.maps.InfoWindow({
					disableAutoPan: true,
		      content: "<div id='place-details'>"+
										 "<h5 id='place-name'></h5>"+
										 "<p id='place-category'></p>"+
										 "<p id='place-address'></p>"+
										 "<p id='place-description'></p>"+
										 "<p id='place-thumbnails'></p>"+
									 "</div>"
		    });
			}

			self.deactivateMarker = function() {
				// Close the inforwindow and the marker icon become blue
				if (self.currentItem()) {
					self.currentItem().infowindow.close();
					self.currentItem().marker.setIcon("http://maps.google.com/mapfiles/ms/micons/blue-dot.png");
				}
			};

			self.activateMarker = function() {
				// Sidebar menu display the selected item
				location.hash = self.currentItem().id;
				// Open infowindow and the the marker icon become red
				self.currentItem().marker.setIcon("http://maps.google.com/mapfiles/ms/micons/red-dot.png");
				self.currentItem().infowindow.open(self.map, self.currentItem().marker);
			};

			self.changeBackground = function() {
				// Change sidebar background with a custom image
				var position = self.currentItem().marker.getPosition();
				var url = "https://maps.googleapis.com/maps/api/staticmap"+
									"?center="+ (position.lat() + 0.001) +","+ (position.lng() + 0.015) +
									"&size=1600x1600&zoom=14&scale=2"+
									"&markers="+ position.lat() +","+ position.lng() +
									"&style=saturation:-100|visibility:simplified&style=feature:all|element:labels|visibility:off" +
									"&key=AIzaSyBECEJTm4eLdS3AZ-a5zelfVVpSCLxHFcY";

				$('#background-image').attr('style', 'background-image: url("'+url+'");')
			};
  };

  return MapViewModel;
});
