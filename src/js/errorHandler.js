define([], function() {

  var errorHandler = function() {
      this.displayError = function(response, defaultErrorMsg, elem="#map") {
        // Retrieve error message and display the error (by default the #map DOM element is replaced)
        var errorMsg, errorContent;

        if (response.meta && response.meta.errorMessage) {
          errorMsg = response.meta.errorMessage;
        } else if (response.responseJSON && response.responseJSON.meta &&
                   response.responseJSON.meta.errorMessage) {
          errorMsg = response.responseJSON.meta.errorMessage;
        } else {
          errorMsg = defaultErrorMsg;
        }

        errorContent = "<div class='error'>"+
               "<h1>Oops, an error occured!</h1>"+
               "<p>"+ errorMsg +"</p>"+
               "<p class='text-center'><a href='' onclick='reload()'>Refresh the page</a></p>"+
               "</div>";

        $(elem).html(errorContent);
      };
    };

    return errorHandler;
});
