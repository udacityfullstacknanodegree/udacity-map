// Require.js allows us to configure shortcut alias
require.config({
	paths: {
		knockout: '/lib/js/knockout'
	}
});

require([
  'knockout',
  'viewmodels/map',
	'errorHandler'
], function(ko, MapViewModel, ErrorHandler) {
  'use strict';

	var mapConfig = {
		// Location : Toronto
		location: {lat: 43.6532, lng: -79.3832},
		zoom: 14,
		// Sport complexes parks and attraction POI are displayed on the map
		styles: [{
			 "featureType": "poi",
			 "elementType": "labels",
			 "stylers": [
				 { "visibility": "off" }
			 ]
		 },
		 {
			 "featureType": "poi.attraction",
			 "stylers": [
				 { "visibility": "on" }
			 ]
		 },
		 {
			 "featureType": "poi.park",
			 "stylers": [
				 { "visibility": "on" },
			 ]
		 },
		 {
			 "featureType": "poi.sports_complex",
			 "stylers": [
				 { "visibility": "on" }
			 ]
		 }],
		 // User can't zoom by scrolling
		 scrollwheel: false,
		 // Disable Streetview
		 streetViewControl: false,
		 // Disable map type
		 mapTypeControl: false
	};


	var mapViewModel;

	try {
		mapViewModel = new MapViewModel(mapConfig);

		// Retrieved places (currently open) from Foursquare API
		mapViewModel.init();

		ko.applyBindings(mapViewModel);
	} catch(e) {
		new ErrorHandler().displayError(e, "Unable to load the map");
	}
});
