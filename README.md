# Neighborhood map

Single page application featuring a map of Toronto and points of interest that are currently open.

## Preview

![Udacity Project](https://i.imgsafe.org/e0686034ff.png)

## Installation

Run the command `npm install` to install project dependancies

Start a web server by running the command `grunt connect:dev:keepalive`

Reach the url [localhost:8000](http://localhost:8000) to display the project
